package kz.astana.menu;

public class Film {

    private int poster, price;
    private String name, about, realise, genre;

    public Film(int poster, String name, String about, String realise, String genre, int price) {
        this.poster = poster;
        this.name = name;
        this.about = about;
        this.realise = realise;
        this.genre = genre;
        this.price = price;
    }

    public int getPoster() {
        return poster;
    }

    public void setPoster(int poster) {
        this.poster = poster;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getRealise() {
        return realise;
    }

    public void setRealise(String realise) {
        this.realise = realise;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
