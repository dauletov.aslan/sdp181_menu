package kz.astana.menu;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class CreateActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create);

        Toolbar toolbar = findViewById(R.id.toolbar);
        EditText nameEditText = findViewById(R.id.nameEditText);
        EditText aboutEditText = findViewById(R.id.aboutEditText);
        EditText realiseEditText = findViewById(R.id.realiseEditText);
        EditText genreEditText = findViewById(R.id.genreEditText);
        EditText priceEditText = findViewById(R.id.priceEditText);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
//        toolbar.getMenu().clear();
//        toolbar.inflateMenu(R.menu.my_menu);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.done:
                        addFilm(
                                nameEditText.getText().toString(),
                                aboutEditText.getText().toString(),
                                realiseEditText.getText().toString(),
                                genreEditText.getText().toString(),
                                priceEditText.getText().toString()
                        );
                        return true;
                    case R.id.second:
                        Toast.makeText(CreateActivity.this, "Second clicked", Toast.LENGTH_SHORT).show();
                        return true;
                    case R.id.third:
                        Toast.makeText(CreateActivity.this, "Third clicked", Toast.LENGTH_SHORT).show();
                        return true;
                    case R.id.firstCategory:
                        Toast.makeText(CreateActivity.this, "First category clicked", Toast.LENGTH_SHORT).show();
                        return true;
                    default:
                        return false;
                }
            }
        });
    }

    private void addFilm(String name, String about, String date, String genre, String price) {
        Intent intent = new Intent();
        intent.putExtra("EXTRA_NAME", name);
        intent.putExtra("EXTRA_ABOUT", about);
        intent.putExtra("EXTRA_DATE", date);
        intent.putExtra("EXTRA_GENRE", genre);
        intent.putExtra("EXTRA_PRICE", price);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }
}