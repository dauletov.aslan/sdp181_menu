package kz.astana.menu;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MainActivity extends AppCompatActivity {

    private RecyclerAdapter recyclerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView recyclerView = findViewById(R.id.recyclerView);

        recyclerAdapter = new RecyclerAdapter(getFilms());
        recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));
        recyclerView.setAdapter(recyclerAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.my_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.create:
                Intent intent = new Intent(MainActivity.this, CreateActivity.class);
                startActivityForResult(intent, 100);
                return true;
            case R.id.basket:
                Toast.makeText(MainActivity.this, "Empty basket", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.exit:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 100) {
                String name = data.getStringExtra("EXTRA_NAME");
                String about = data.getStringExtra("EXTRA_ABOUT");
                String date = data.getStringExtra("EXTRA_DATE");
                String genre = data.getStringExtra("EXTRA_GENRE");
                String price = data.getStringExtra("EXTRA_PRICE");

                Film film = new Film(
                        R.drawable.film_knife,
                        name,
                        about,
                        date,
                        genre,
                        Integer.parseInt(price)
                );

                recyclerAdapter.addItem(film);
            } else if (requestCode == 200) {

            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private ArrayList<Film> getFilms() {
        ArrayList<Film> films = new ArrayList<Film>();

        films.add(
                new Film(
                        R.drawable.film_actress,
                        "Actress",
                        "The first recorded case of a performing actor occurred in 534 BC (though the changes in calendar over the years make it hard to determine exactly) when the Greek performer Thespis stepped onto the stage at the Theatre Dionysus to become the first known person to speak words as a character in a play or story.",
                        "2019",
                        "Comedy",
                        1000
                )
        );
        films.add(
                new Film(
                        R.drawable.film_age_limit,
                        "Age limit",
                        "Film about age",
                        "2010",
                        "Drama",
                        500
                )
        );
        films.add(
                new Film(
                        R.drawable.film_dressing_table,
                        "Dressing table",
                        "About dressing table",
                        "2018",
                        "Comedy",
                        100
                )
        );
        films.add(
                new Film(
                        R.drawable.film_red_carpet,
                        "Red carpet",
                        "Action film about red carpet",
                        "2019",
                        "Action",
                        2000
                )
        );
        films.add(
                new Film(
                        R.drawable.film_spotlight,
                        "Spotlight",
                        "About agent 007",
                        "2005",
                        "Action",
                        2000
                )
        );
        films.add(
                new Film(
                        R.drawable.film_video_player,
                        "Video player",
                        "About video player",
                        "2010",
                        "Animation",
                        1000
                )
        );

        return films;
    }
}