package kz.astana.menu;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.RecyclerViewHolder> {

    private ArrayList<Film> films;

    public RecyclerAdapter(ArrayList<Film> films) {
        this.films = films;
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_recycler, parent, false);
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder holder, int position) {
        Film film = films.get(position);

        holder.poster.setImageResource(film.getPoster());
        holder.name.setText(film.getName());
        holder.about.setText(film.getAbout());
    }

    @Override
    public int getItemCount() {
        return films.size();
    }

    public void addItem(Film film) {
        films.add(film);
        notifyDataSetChanged();
    }

    static class RecyclerViewHolder extends RecyclerView.ViewHolder {

        public ImageView poster;
        public TextView name, about;

        public RecyclerViewHolder(@NonNull View itemView) {
            super(itemView);
            poster = itemView.findViewById(R.id.posterImageView);
            name = itemView.findViewById(R.id.nameTextView);
            about = itemView.findViewById(R.id.aboutTextView);
        }
    }
}
